#!/usr/bin/bash

while true; do
	read -p "Project name: " projectname
	if [[ -z $projectname ]]; then
		echo "Project name is empty. Please try again or hit Ctrl+C."
	else
		break
	fi
done


sed "s/placeholder-uuid/$(uuidgen)/g" -i firmware/firmware.ino
mv firmware/firmware.ino "$projectname".ino
mv firmware "$projectname"
