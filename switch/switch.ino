#define BAUD 9600

#define DEVICE_UUID "938f0e00-d7b9-4cd1-a757-16754e123306"
#define BOARD_VERSION "1.0.0"


enum {
  COMMAND_INFO = 0,
  COMMAND_RESET,
  COMMAND_ERROR_CODE,
  COMMAND_ON,
  COMMAND_OFF,
  COMMAND_BURST,
  COMMAND_SENTINEL
};

enum {
  ERR_NONE = 0,
  ERR_COMMAND_NOT_FOUND,
  ERR_BUTTON_NOT_FOUND,
  ERR_SENTINEL
};



uint8_t error_code;

#define DEFAULT_BURST_DURATION 1000
#define BUTTONS 2

const uint8_t switch_pins[] = {
	2,
	3
};

const uint16_t burst_duration[] = {
	DEFAULT_BURST_DURATION,
	DEFAULT_BURST_DURATION
};


void info() {
  // number of lines, simplifies parsing on receiving side
  Serial.write(5);
  // ID
  Serial.write(DEVICE_UUID);
  // board version
  Serial.write(BOARD_VERSION);
  // add commands here
  Serial.write("COMMAND_INFO COMMAND_RESET COMMAND_ERROR_CODE COMMAND_BURST");
  // date & time of compilation
  Serial.println(__DATE__);
  Serial.println(__TIME__);
}


void reset() {
  error_code = 0;
}


void setup() {
  error_code = 0;
  Serial.begin(BAUD);

  for (int i = 0; i < BUTTONS; ++i) {
    pinMode(switch_pins[i], OUTPUT);
  }
}


void loop() {
  while (Serial.available() == 0) {}

  uint8_t command = Serial.read();

  // split command into command & payload
  uint8_t payload = command >> 4;
  command = command & 0xF;
  
  switch (command) {
    case COMMAND_INFO:
    {
      info();
    }
    break;

    case COMMAND_RESET:
    {
      reset();
    }
    break;

    case COMMAND_ERROR_CODE:
    {
      Serial.write(error_code);
      error_code = 0;
    }
    break;

    case COMMAND_ON:
    {
      if (payload < BUTTONS) {
        digitalWrite(switch_pins[payload], HIGH);
      }
      else {
        error_code = ERR_BUTTON_NOT_FOUND;
      }
    }
    break;

    case COMMAND_OFF:
    {
      if (payload < BUTTONS) {
        digitalWrite(switch_pins[payload], LOW);
      }
      else {
        error_code = ERR_BUTTON_NOT_FOUND;
      }
    }
    break;

  	case COMMAND_BURST:
  	{
  		if (payload < BUTTONS) {
  			digitalWrite(switch_pins[payload], HIGH);
  			delay(burst_duration[payload]);
  			digitalWrite(switch_pins[payload], LOW);
  		}
  		else {
  			error_code = ERR_BUTTON_NOT_FOUND;
  		}
  	}
  	break;
  
      default:
      {
        error_code = ERR_COMMAND_NOT_FOUND;
      }
      break;
    }
}
