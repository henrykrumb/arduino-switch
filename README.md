# arduino-firmware-template

## Rationale


## Usage
The default template provides the following commands:
* COMMAND\_INFO: Obtain hardware information string.
* COMMAND\_RESET: Perform soft reset.
* COMMAND\_ERROR\_CODE: Retrieve error code.

Currently, the default template implements one error code, that is
"ERR\_COMMAND\_NOT\_FOUND", which is set in case an undefined command was
called.

